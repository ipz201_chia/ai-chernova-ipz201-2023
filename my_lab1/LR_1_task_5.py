import pandas as pd
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import f1_score
from sklearn.metrics import roc_curve
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score


def chernova_find_TP(y_true, y_pred):
    # counts the number of true positives (y_true = 1, y_pred = 1)
    return sum((y_true == 1) & (y_pred == 1))


def chernova_find_FN(y_true, y_pred):
    # counts the number of false negatives (y_true = 1, y_pred = 0)
    return sum((y_true == 1) & (y_pred == 0))


def chernova_find_FP(y_true, y_pred):
    # counts the number of false positives (y_true = 0, y_pred = 1)
    return sum((y_true == 0) & (y_pred == 1))


def chernova_find_TN(y_true, y_pred):
    # counts the number of true negatives (y_true = 0, y_pred = 0)
    return sum((y_true == 0) & (y_pred == 0))


def chernova_find_conf_matrix_values(y_true, y_pred):
    # calculate TP, FN, FP, TN
    TP = chernova_find_TP(y_true, y_pred)
    FN = chernova_find_FN(y_true, y_pred)
    FP = chernova_find_FP(y_true, y_pred)
    TN = chernova_find_TN(y_true, y_pred)
    return TP, FN, FP, TN


def chernova_confusion_matrix(y_true, y_pred):
    TP, FN, FP, TN = chernova_find_conf_matrix_values(y_true, y_pred)
    return np.array([[TN, FP], [FN, TP]])


def chernova_accuracy_score(y_true, y_pred):  # calculates the fraction of samples
    TP, FN, FP, TN = chernova_find_conf_matrix_values(y_true, y_pred)
    return (TP + TN) / (TP + TN + FP + FN)


def chernova_recall_score(y_true, y_pred):
    # calculates the fraction of positive samples predicted correctly
    TP, FN, FP, TN = chernova_find_conf_matrix_values(y_true, y_pred)
    return TP / (TP + FN)


def chernova_precision_score(y_true, y_pred):
    # calculates the fraction of predicted positives samples that are actually positive
    TP, FN, FP, TN = chernova_find_conf_matrix_values(y_true, y_pred)
    return TP / (TP + FP)


def chernova_f1_score(y_true, y_pred):  # calculates the F1 score
    recall = chernova_recall_score(y_true, y_pred)
    precision = chernova_precision_score(y_true, y_pred)
    return (2 * (precision * recall)) / (precision + recall)


if __name__ == '__main__':
    df = pd.read_csv('data_metrics.csv')
    df.head()

    thresh = 0.5
    df['predicted_RF'] = (df.model_RF >= 0.5).astype('int')
    df['predicted_LR'] = (df.model_LR >= 0.5).astype('int')
    df.head()

    print("sklearn confusion matrix:\n", confusion_matrix(df.actual_label.values, df.predicted_RF.values))

    print("\nown calculation:")
    print('TP:', chernova_find_TP(df.actual_label.values, df.predicted_RF.values))
    print('FN:', chernova_find_FN(df.actual_label.values, df.predicted_RF.values))
    print('FP:', chernova_find_FP(df.actual_label.values, df.predicted_RF.values))
    print('TN:', chernova_find_TN(df.actual_label.values, df.predicted_RF.values))

    print("\nchernova confusion matrix:\n", chernova_confusion_matrix(df.actual_label.values, df.predicted_RF.values))

    assert np.array_equal(chernova_confusion_matrix(df.actual_label.values, df.predicted_RF.values),
                          confusion_matrix(df.actual_label.values,
                                           df.predicted_RF.values)), 'my_confusion_matrix() is not correct for RF'
    assert np.array_equal(chernova_confusion_matrix(df.actual_label.values, df.predicted_LR.values),
                          confusion_matrix(df.actual_label.values,
                                           df.predicted_LR.values)), 'my_confusion_matrix() is not correct for LR'

    print("\n\nsklearn accuracy score RF:", accuracy_score(df.actual_label.values, df.predicted_RF.values))
    print("sklearn LR accuracy score RL:", accuracy_score(df.actual_label.values, df.predicted_LR.values))

    assert chernova_accuracy_score(df.actual_label.values, df.predicted_RF.values) == accuracy_score(
        df.actual_label.values, df.predicted_RF.values), 'my_accuracy_score failed on RF'
    assert chernova_accuracy_score(df.actual_label.values, df.predicted_LR.values) == accuracy_score(
        df.actual_label.values, df.predicted_LR.values), 'my_accuracy_score failed on LR'

    print('chernova accuracy RF:%.3f' % (chernova_accuracy_score(df.actual_label.values, df.predicted_RF.values)))
    print('chernova accuracy LR:%.3f' % (chernova_accuracy_score(df.actual_label.values, df.predicted_LR.values)))

    print("\nsklearn recall score RF:", recall_score(df.actual_label.values, df.predicted_RF.values))
    print("sklearn recall score LR:", recall_score(df.actual_label.values, df.predicted_LR.values))

    assert chernova_recall_score(df.actual_label.values, df.predicted_RF.values) == recall_score(
        df.actual_label.values, df.predicted_RF.values), 'my_accuracy_score failed on RF'
    assert chernova_recall_score(df.actual_label.values, df.predicted_LR.values) == recall_score(
        df.actual_label.values, df.predicted_LR.values), 'my_accuracy_score failed on LR'

    print('Recall RF: %.3f' % (chernova_recall_score(df.actual_label.values, df.predicted_RF.values)))
    print('Recall LR: %.3f' % (chernova_recall_score(df.actual_label.values, df.predicted_LR.values)))

    print("\nsklearn precision score RF:", precision_score(df.actual_label.values, df.predicted_RF.values))
    print("sklearn precision score LR:", precision_score(df.actual_label.values, df.predicted_LR.values))

    assert chernova_precision_score(df.actual_label.values, df.predicted_RF.values) == precision_score(
        df.actual_label.values, df.predicted_RF.values), 'my_accuracy_score failed on RF'
    assert chernova_precision_score(df.actual_label.values, df.predicted_LR.values) == precision_score(
        df.actual_label.values, df.predicted_LR.values), 'my_accuracy_score failed on LR'

    print('Precision RF:%.3f' % (chernova_precision_score(df.actual_label.values, df.predicted_RF.values)))
    print('Precision LR:%.3f' % (chernova_precision_score(df.actual_label.values, df.predicted_LR.values)))

    print("\nsklearn f1 score RF:", f1_score(df.actual_label.values, df.predicted_RF.values))
    print("sklearn f1 score LR:", f1_score(df.actual_label.values, df.predicted_LR.values))

    assert chernova_f1_score(df.actual_label.values, df.predicted_RF.values) == f1_score(
        df.actual_label.values, df.predicted_RF.values), 'my_accuracy_score failed on RF'
    assert chernova_f1_score(df.actual_label.values, df.predicted_LR.values) == f1_score(
        df.actual_label.values, df.predicted_LR.values), 'my_accuracy_score failed on LR'

    print('F1 RF: %.3f' % (chernova_f1_score(df.actual_label.values, df.predicted_RF.values)))
    print('F1 LR: %.3f' % (chernova_f1_score(df.actual_label.values, df.predicted_LR.values)))

    print('\n\nRF\nscores with threshold = 0.5')
    print('Accuracy RF:%.3f' % (chernova_accuracy_score(df.actual_label.values, df.predicted_RF.values)))
    print('Recall RF: %.3f' % (chernova_recall_score(df.actual_label.values, df.predicted_RF.values)))
    print('Precision RF: % .3f' % (chernova_precision_score(df.actual_label.values, df.predicted_RF.values)))
    print('F1 RF: %.3f' % (chernova_f1_score(df.actual_label.values, df.predicted_RF.values)))
    print('\nscores with threshold = 0.25')
    print('Accuracy RF:% .3f' % (
        chernova_accuracy_score(df.actual_label.values, (df.model_RF >= 0.25).astype('int').values)))
    print(
        'Recall RF: %.3f' % (chernova_recall_score(df.actual_label.values, (df.model_RF >= 0.25).astype('int').values)))
    print('Precision RF:%.3f' % (
        chernova_precision_score(df.actual_label.values, (df.model_RF >= 0.25).astype('int').values)))
    print('F1 RF: %.3f' % (chernova_f1_score(df.actual_label.values, (df.model_RF >= 0.25).astype('int').values)))

    print('\nLR\nscores with threshold = 0.5')
    print('Accuracy LR:%.3f' % (chernova_accuracy_score(df.actual_label.values, df.predicted_LR.values)))
    print('Recall LR: %.3f' % (chernova_recall_score(df.actual_label.values, df.predicted_LR.values)))
    print('Precision LR: % .3f' % (chernova_precision_score(df.actual_label.values, df.predicted_LR.values)))
    print('F1 LR: %.3f' % (chernova_f1_score(df.actual_label.values, df.predicted_LR.values)))
    print('\nscores with threshold = 0.25')
    print('Accuracy LR:% .3f' % (
        chernova_accuracy_score(df.actual_label.values, (df.model_LR >= 0.25).astype('int').values)))
    print(
        'Recall LR: %.3f' % (chernova_recall_score(df.actual_label.values, (df.model_LR >= 0.25).astype('int').values)))
    print('Precision LR: %.3f' % (
        chernova_precision_score(df.actual_label.values, (df.model_LR >= 0.25).astype('int').values)))
    print('F1 LR: %.3f' % (chernova_f1_score(df.actual_label.values, (df.model_LR >= 0.25).astype('int').values)))

    print("\n------------------------------------")
    fpr_RF, tpr_RF, thresholds_RF = roc_curve(df.actual_label.values, df.model_RF.values)
    fpr_LR, tpr_LR, thresholds_LR = roc_curve(df.actual_label.values, df.model_LR.values)

    plt.plot(fpr_RF, tpr_RF, 'r-', label='RF')
    plt.plot(fpr_LR, tpr_LR, 'b-', label='LR')
    plt.plot([0, 1], [0, 1], 'k-', label='random')
    plt.plot([0, 0, 1, 1], [0, 1, 1, 1], 'g-', label='perfect')
    plt.legend()
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.show()

    auc_RF = roc_auc_score(df.actual_label.values, df.model_RF.values)
    auc_LR = roc_auc_score(df.actual_label.values, df.model_LR.values)
    print('AUC RF:%.3f' % auc_RF)
    print('AUC LR:%.3f' % auc_LR)

    plt.plot(fpr_RF, tpr_RF, 'r-', label='RF AUC: %.3f' % auc_RF)
    plt.plot(fpr_LR, tpr_LR, 'b-', label='LR AUC: %.3f' % auc_LR)
    plt.plot([0, 1], [0, 1], 'k-', label='random')
    plt.plot([0, 0, 1, 1], [0, 1, 1, 1], 'g-', label='perfect')
    plt.legend()
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.show()
