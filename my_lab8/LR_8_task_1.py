import cv2

if __name__ == '__main__':
    # LOAD AN IMAGE USING 'IMREAD'
    img = cv2.imread("chernova_photo.jpg")
    # DISPLAY
    cv2.imshow("Chernova", img)
    cv2.waitKey(0)