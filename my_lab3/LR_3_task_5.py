import pickle
import numpy as np
from sklearn import linear_model
import sklearn.metrics as sm
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures

if __name__ == '__main__':
    m = 100
    X = np.linspace(-3, 3, m)
    y = 4 + np.sin(X) + np.random.uniform(-0.6, 0.6, m)
    X = X.reshape(-1, 1)
    y = y.reshape(-1, 1)

    # Створення графіку залежності у=f(X)
    plt.plot(X, y, color='red')
    plt.show()

    poly_features = PolynomialFeatures(degree=2, include_bias=False)
    X_poly = poly_features.fit_transform(X)

    print(X[1], y[1])
    print("X[0]:", X[0])
    print("X_poly", X_poly)

    linear_regression = linear_model.LinearRegression()
    linear_regression.fit(X_poly, y)
    print(linear_regression.intercept_, linear_regression.coef_)
    y_pred = linear_regression.predict(X_poly)

    fig, ax = plt.subplots()
    ax.scatter(X, y, edgecolors=(0, 0, 0))
    plt.plot(X, y_pred, color='red', linewidth=2)
    plt.show()










