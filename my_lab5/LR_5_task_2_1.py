import numpy as np

from LR_5_task_1 import Neuron


class ChernovaNeuralNetwork:
    def __init__(self):
        weights = np.array([0, 1])
        bias = 0

        # Класс Neuron із попереднього завдання
        self.h1 = Neuron(weights, bias)
        self.h2 = Neuron(weights, bias)
        self.o1 = Neuron(weights, bias)

    def feedforward(self, x):
        out_h1 = self.h1.feedforward(x)
        out_h2 = self.h2.feedforward(x)

        # Входи для о1 є виходами h1 и h2
        out_o1 = self.o1.feedforward(np.array([out_h1, out_h2]))

        return out_o1


if __name__ == '__main__':
    network = ChernovaNeuralNetwork()
    x = np.array([2, 3])
    print(network.feedforward(x))  # 0.7216325609518421
