from sklearn.datasets import load_iris
iris_dataset = load_iris()

if __name__ == '__main__':
    print("Ключі iris dataset : \n{}".format(iris_dataset.keys()))
    print(iris_dataset["DESCR"][:193] + "\n...")
    print("Назви відповідей: {}".format(iris_dataset["target_names"]))

    print("Назви ознак: \n{}".format(iris_dataset["feature_names"]))
    print("Тип масиву date: {}".format(type(iris_dataset["data"])))
    print("Форма масиву data: {}".format(iris_dataset["data"].shape))
    print("Тип масиву target: {}".format(type(iris_dataset['target'])))
    print("Відповіді:\n{}".format(iris_dataset['target']))